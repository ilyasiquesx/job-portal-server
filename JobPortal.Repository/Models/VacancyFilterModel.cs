using JobPortal.Domain.Common;

namespace JobPortal.Repository.Models
{
    public class VacancyFilterModel
    {
        public string Name { get; set; }
        public long? EmployerId { get; set; }
        public decimal? SalaryFrom { get; set; }
        public decimal? SalaryTo { get; set; }
        public Experience? RequiredExperience { get; set; }
    }
}