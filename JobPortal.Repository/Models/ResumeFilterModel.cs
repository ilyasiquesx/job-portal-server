﻿using JobPortal.Domain.Common;

namespace JobPortal.Repository.Models
{
    public class ResumeFilterModel
    {
        public string Name { get; set; }
        public long? ApplicantId { get; set; }
        public decimal? SalaryFrom { get; set; }
        public Experience? Experience { get; set; }
        public decimal? SalaryTo { get; set; }
    }
}