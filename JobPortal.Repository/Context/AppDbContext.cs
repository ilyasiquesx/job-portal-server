using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using JobPortal.Domain.Abstractions;
using JobPortal.Domain.Tables;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace JobPortal.Repository.Context
{
    public class AppDbContext : IdentityDbContext<User>
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Resume> Resumes { get; set; }
        public DbSet<Vacancy> Vacancies { get; set; }
        public DbSet<Applicant> Applicants { get; set; }
        public DbSet<Employer> Employers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Employer>().HasOne(e => e.User).WithOne(u => u.Employer)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Applicant>().HasOne(a => a.User).WithOne(u => u.Applicant)
                .OnDelete(DeleteBehavior.Cascade);

            base.OnModelCreating(builder);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var addedAuditedEntities = ChangeTracker.Entries<IAuditedEntity>()
                .Where(p => p.State == EntityState.Added)
                .Select(p => p.Entity);

            var modifiedAuditedEntities = ChangeTracker.Entries<IAuditedEntity>()
                .Where(p => p.State == EntityState.Modified)
                .Select(p => p.Entity);

            var now = DateTime.Now;

            foreach (var added in addedAuditedEntities)
            {
                added.CreatedAt = now;
                added.UpdatedAt = now;
            }

            foreach (var modified in modifiedAuditedEntities)
            {
                modified.UpdatedAt = now;
            }

            return base.SaveChangesAsync(cancellationToken);
            
        }
    }
}
