﻿using System.Collections.Generic;
using System.Threading.Tasks;
using JobPortal.Domain.Tables;

namespace JobPortal.Repository.Abstractions
{
    public interface IEmployerRepository : IRepository<Employer>
    {
        public Task<Employer> GetByUserIdAsync(string userId);
        public IEnumerable<Employer> GetEmployersByIdList(IEnumerable<long> ids);
    }
}