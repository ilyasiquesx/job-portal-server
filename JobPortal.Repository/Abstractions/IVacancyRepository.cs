﻿using System.Linq;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Models;

namespace JobPortal.Repository.Abstractions
{
    public interface IVacancyRepository : IRepository<Vacancy>
    {
        IQueryable<Vacancy> GetFilteredVacancies(VacancyFilterModel model);
    }
}