﻿using System.Collections.Generic;
using System.Threading.Tasks;
using JobPortal.Domain.Tables;

namespace JobPortal.Repository.Abstractions
{
    public interface IApplicantRepository : IRepository<Applicant>
    {
        public Task<Applicant> GetByUserIdAsync(string userId);
        public IEnumerable<Applicant> GetApplicantsByIdList(IEnumerable<long> ids);
    }
}