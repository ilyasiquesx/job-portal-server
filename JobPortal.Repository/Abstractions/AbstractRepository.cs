﻿using JobPortal.Repository.Context;

namespace JobPortal.Repository.Abstractions
{
    public abstract class AbstractRepository
    {
        protected readonly AppDbContext AppDbContext;

        protected AbstractRepository(AppDbContext appDbContext)
        {
            AppDbContext = appDbContext;
        }
    }
}