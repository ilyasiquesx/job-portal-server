﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace JobPortal.Repository.Abstractions
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        Task<T> GetByIdAsync(long id);
        Task DeleteAsync(T model);
        Task UpdateAsync(T model);
        Task CreateAsync(T model);
        void LoadExplicit(T model);
    }
}