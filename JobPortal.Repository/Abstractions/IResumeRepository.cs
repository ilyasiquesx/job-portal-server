﻿using System.Linq;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Models;

namespace JobPortal.Repository.Abstractions
{
    public interface IResumeRepository : IRepository<Resume>
    {
        IQueryable<Resume> GetFilteredResumes(ResumeFilterModel model);
    }
}