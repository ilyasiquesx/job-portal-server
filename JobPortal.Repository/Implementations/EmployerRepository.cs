﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace JobPortal.Repository.Implementations
{
    public class EmployerRepository : AbstractRepository, IEmployerRepository
    {
        public EmployerRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public IEnumerable<Employer> GetAll()
        {
            return AppDbContext.Employers.ToList();
        }

        public async Task<Employer> GetByIdAsync(long id)
        {
            return await AppDbContext.Employers.FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task DeleteAsync(Employer model)
        {
            AppDbContext.Employers.Remove(model);
            await AppDbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(Employer model)
        {
            await AppDbContext.SaveChangesAsync();
        }

        public async Task CreateAsync(Employer model)
        {
            AppDbContext.Employers.Add(model);
            await AppDbContext.SaveChangesAsync();
        }

        public void LoadExplicit(Employer model)
        {
            AppDbContext.Entry(model).Collection(e => e.Vacancies).Load();
        }

        public Task<Employer> GetByUserIdAsync(string userId)
        {
            return AppDbContext.Employers.FirstOrDefaultAsync(e => e.UserId == userId);
        }

        public IEnumerable<Employer> GetEmployersByIdList(IEnumerable<long> ids)
        {
            return AppDbContext.Employers.Where(e => ids.Contains(e.Id));
        }
    }
}