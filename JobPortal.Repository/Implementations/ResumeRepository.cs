﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Repository.Context;
using JobPortal.Repository.Models;
using Microsoft.EntityFrameworkCore;

namespace JobPortal.Repository.Implementations
{
    public class ResumeRepository : AbstractRepository, IResumeRepository
    {
        public ResumeRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public IEnumerable<Resume> GetAll()
        {
            return AppDbContext.Resumes.ToList();
        }

        public async Task<Resume> GetByIdAsync(long id)
        {
            return await AppDbContext.Resumes.FirstOrDefaultAsync(r => r.Id == id);
        }

        public async Task DeleteAsync(Resume model)
        {
            AppDbContext.Resumes.Remove(model);
            await AppDbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(Resume model)
        {
            AppDbContext.Resumes.Update(model);
            await AppDbContext.SaveChangesAsync();
        }

        public async Task CreateAsync(Resume model)
        {
            AppDbContext.Resumes.Add(model);
            await AppDbContext.SaveChangesAsync();
        }

        public void LoadExplicit(Resume model)
        {
            AppDbContext.Entry(model).Reference(r => r.Applicant).Load();
        }

        public IQueryable<Resume> GetFilteredResumes(ResumeFilterModel model)
        {
            var resumes = AppDbContext.Resumes.AsQueryable();
            if (model?.Name != null)
            {
                resumes = resumes.Where(v => v.Name.ToLower().StartsWith(model.Name.ToLower()));
            }

            if (model?.ApplicantId != null)
            {
                resumes = resumes.Where(v => v.ApplicantId == model.ApplicantId);
            }

            if (model?.SalaryFrom != null)
            {
                resumes = resumes.Where(v =>
                    v.SalaryFrom >= model.SalaryFrom || (v.SalaryFrom == null && v.SalaryTo == null));
            }

            if (model?.SalaryTo != null)
            {
                resumes = resumes.Where(v =>
                    v.SalaryTo <= model.SalaryTo || (v.SalaryFrom == null && v.SalaryTo == null));
            }

            if (model?.Experience != null)
            {
                resumes = resumes.Where(v => v.Experience == model.Experience);
            }

            return resumes;
        }
    }
}