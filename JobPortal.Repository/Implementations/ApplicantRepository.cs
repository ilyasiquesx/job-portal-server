﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace JobPortal.Repository.Implementations
{
    public class ApplicantRepository : AbstractRepository, IApplicantRepository
    {
        public ApplicantRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public IEnumerable<Applicant> GetAll()
        {
            return AppDbContext.Applicants.ToList();
        }

        public async Task<Applicant> GetByIdAsync(long id)
        {
            return await AppDbContext.Applicants.FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task DeleteAsync(Applicant model)
        {
            AppDbContext.Applicants.Remove(model);
            await AppDbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(Applicant model)
        {
            AppDbContext.Applicants.Update(model);
            await AppDbContext.SaveChangesAsync();
        }

        public async Task CreateAsync(Applicant model)
        {
            AppDbContext.Applicants.Add(model);
            await AppDbContext.SaveChangesAsync();
        }

        public void LoadExplicit(Applicant model)
        {
            AppDbContext.Entry(model).Collection(a => a.Resumes).Load();
        }

        public async Task<Applicant> GetByUserIdAsync(string userId)
        {
            return await AppDbContext.Applicants.FirstOrDefaultAsync(a => a.UserId == userId);
        }

        public IEnumerable<Applicant> GetApplicantsByIdList(IEnumerable<long> ids)
        {
            return AppDbContext.Applicants.Where(a => ids.Contains(a.Id));
        }
    }
}