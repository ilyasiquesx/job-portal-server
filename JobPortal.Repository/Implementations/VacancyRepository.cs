﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Repository.Context;
using JobPortal.Repository.Models;
using Microsoft.EntityFrameworkCore;

namespace JobPortal.Repository.Implementations
{
    public class VacancyRepository : AbstractRepository, IVacancyRepository
    {
        public VacancyRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public IEnumerable<Vacancy> GetAll()
        {
            return AppDbContext.Vacancies.ToList();
        }

        public async Task<Vacancy> GetByIdAsync(long id)
        {
            return await AppDbContext.Vacancies.FirstOrDefaultAsync(v => v.Id == id);
        }

        public async Task DeleteAsync(Vacancy model)
        {
            AppDbContext.Vacancies.Remove(model);
            await AppDbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(Vacancy model)
        {
            AppDbContext.Vacancies.Update(model);
            await AppDbContext.SaveChangesAsync();
        }

        public async Task CreateAsync(Vacancy model)
        {
            AppDbContext.Vacancies.Add(model);
            await AppDbContext.SaveChangesAsync();
        }

        public void LoadExplicit(Vacancy model)
        {
            AppDbContext.Entry(model).Reference(v => v.Employer).Load();
        }

        public IQueryable<Vacancy> GetFilteredVacancies(VacancyFilterModel filterModel)
        {
            var vacancies = AppDbContext.Vacancies.AsQueryable();
            if (filterModel?.Name != null)
            {
                vacancies = vacancies.Where(v => v.Name.ToLower().StartsWith(filterModel.Name.ToLower()));
            }

            if (filterModel?.EmployerId != null)
            {
                vacancies = vacancies.Where(v => v.EmployerId == filterModel.EmployerId);
            }

            if (filterModel?.SalaryFrom != null)
            {
                vacancies = vacancies.Where(v =>
                    v.SalaryFrom >= filterModel.SalaryFrom || (v.SalaryFrom == null && v.SalaryTo == null));
            }

            if (filterModel?.SalaryTo != null)
            {
                vacancies = vacancies.Where(v =>
                    v.SalaryTo <= filterModel.SalaryTo || (v.SalaryFrom == null && v.SalaryTo == null));
            }

            if (filterModel?.RequiredExperience != null)
            {
                vacancies = vacancies.Where(v => v.RequiredExperience == filterModel.RequiredExperience);
            }

            return vacancies;
        }
    }
}