﻿using System.Threading.Tasks;
using JobPortal.Service.Commands.Resumes;
using JobPortal.Service.Helpers;
using JobPortal.Service.Queries.Resumes;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JobPortal.WebApi.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ResumeController : ApiController
    {
        private readonly IMediator _mediator;

        public ResumeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Метод создания резюме
        /// </summary>
        /// <param name="command">Тело запроса</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = Roles.AdminOrApplicant)]
        public async Task<IActionResult> Create(CreateResumeCommand command)
        {
            command.Username = User!.Identity!.Name;
            var response = await _mediator.Send(command);
            if (response.IsSucceeded)
            {
                return Ok(response.Data);
            }

            return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }

        /// <summary>
        /// Метод удаления резюме
        /// </summary>
        /// <param name="id">Идентификатор резюме</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.AdminOrApplicant)]
        public async Task<IActionResult> Delete(long id)
        {
            var response = await _mediator.Send(new DeleteResumeCommand {Username = User!.Identity!.Name, Id = id});
            if (response.IsSucceeded)
            {
                return NoContent();
            }

            return StatusCode(response.StatusCode, new {erros = response.ErrorMessages});
        }

        /// <summary>
        /// Метод обновления резюме
        /// </summary>
        /// <param name="command">Тело запроса</param>
        /// <returns></returns>
        [HttpPut]
        [Authorize(Roles = Roles.AdminOrApplicant)]
        public async Task<IActionResult> Update(UpdateResumeCommand command)
        {
            command.Username = User!.Identity!.Name;
            var response = await _mediator.Send(command);
            if (response.IsSucceeded)
            {
                return NoContent();
            }

            return StatusCode(response.StatusCode, new {erros = response.ErrorMessages});
        }
        
        /// <summary>
        /// Метод получения списка резюме, удовлетворяющих критериям фильтрации
        /// </summary>
        /// <param name="query">Параметры фильтрации</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetFiltered([FromQuery] GetResumesQuery query)
        {
            var response = await _mediator.Send(query);
            if (response.IsSucceeded)
            {
                return Ok(response.Data);
            }
            
            return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }
        
        /// <summary>
        /// Метод получения резюме
        /// </summary>
        /// <param name="id">Идентификатор резюме</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var query = new GetResumeQuery {Id = id};
            var response = await _mediator.Send(query);
            if (response.IsSucceeded)
            {
                return Ok(response.Data);
            }
            
            return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }
    }
}