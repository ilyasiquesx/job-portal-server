﻿using System.Threading.Tasks;
using JobPortal.Service.Commands.Account;
using JobPortal.Service.Queries.Account;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JobPortal.WebApi.Controllers
{
    public class AccountController : ApiController
    {
        private readonly IMediator _mediator;

        public AccountController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Метод регистрации нового пользователя в системе
        /// </summary>
        /// <param name="command">Модель запроса</param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(Register))]
        public async Task<IActionResult> Register(RegisterCommand command)
        {
            var response = await _mediator.Send(command);
            if (!response.IsSucceeded)
            {
                return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
            }
            
            var loginCommand = new LoginQuery
            {
                Username = command.Username,
                Password = command.Password
            };
            
            return await Login(loginCommand);
        }

        /// <summary>
        /// Метод получения токена доступа к защищенным ресурсам
        /// </summary>
        /// <param name="model">Модель запроса</param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(Login))]
        public async Task<IActionResult> Login([FromBody] LoginQuery model)
        {
            var response = await _mediator.Send(model);
            return response.IsSucceeded
                ? Ok(response.Data)
                : StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }
    }
}