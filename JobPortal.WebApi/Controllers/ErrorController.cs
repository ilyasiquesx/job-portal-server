﻿using JobPortal.Service.Responses.Error;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace JobPortal.WebApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorController : ApiController
    {
        private readonly ILogger<ErrorController> _logger;
        private const int InternalServerErrorCode = StatusCodes.Status500InternalServerError;

        public ErrorController(ILogger<ErrorController> logger)
        {
            _logger = logger;
        }

        public ErrorResponse Error()
        {
            Response.StatusCode = InternalServerErrorCode; 
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context.Error;
            
            _logger.LogError(exception, "Got unhandled exception.");
            
            return new ErrorResponse
            {
                TraceIdentifier = HttpContext.TraceIdentifier
            };
        }
    }
}