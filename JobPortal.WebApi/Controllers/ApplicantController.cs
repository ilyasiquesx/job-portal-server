﻿using System.Threading.Tasks;
using JobPortal.Service.Commands.Applicants;
using JobPortal.Service.Helpers;
using JobPortal.Service.Queries.Applicants;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JobPortal.WebApi.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ApplicantController : ApiController
    {
        private readonly IMediator _mediator;

        public ApplicantController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Метод получения профиля соискателя
        /// </summary>
        /// <param name="id">Идентификатор соискателя</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var response = await _mediator.Send(new GetApplicantQuery {Id = id});
            if (response.IsSucceeded)
            {
                return Ok(response.Data);
            }

            return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }
        
        /// <summary>
        /// Метод получения профиля соискателя для выбранного пользователя
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns></returns>
        [HttpGet("user/{userId}")]
        public async Task<IActionResult> GetByUserId(string userId)
        {
            var response = await _mediator.Send(new GetApplicantByUserIdQuery {UserId = userId});
            if (response.IsSucceeded)
            {
                return Ok(response.Data);
            }

            return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }
        
        /// <summary>
        /// Метод создания профиля соискателя
        /// </summary>
        /// <param name="command">Тело запроса</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = Roles.AdminOrApplicant)]
        public async Task<IActionResult> Create(CreateApplicantCommand command)
        {
            command.Username = User!.Identity!.Name;
            var response = await _mediator.Send(command);
            if (response.IsSucceeded)
            {
                return StatusCode(response.StatusCode, response.Data);
            }

            return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }
        
        /// <summary>
        /// Метод обновления профиля соискателя
        /// </summary>
        /// <param name="command">Тело запроса</param>
        /// <returns></returns>
        [HttpPut]
        [Authorize(Roles = Roles.AdminOrApplicant)]
        public async Task<IActionResult> Update(UpdateApplicantCommand command)
        {
            command.Username = User!.Identity!.Name;
            var response = await _mediator.Send(command);
            if (response.IsSucceeded)
            {
                return NoContent();
            }

            return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }

        /// <summary>
        /// Метод удаления профиля соискателя
        /// </summary>
        /// <param name="id">Идентификатор соискателя</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.AdminOrApplicant)]
        public async Task<IActionResult> Delete(long id)
        {
            var command = new DeleteApplicantCommand {Id = id, Username = User!.Identity!.Name};
            var response = await _mediator.Send(command);
            if (response.IsSucceeded)
            {
                return NoContent();
            }

            return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }
    }
}