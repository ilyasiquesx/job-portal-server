﻿using System.Threading.Tasks;
using JobPortal.Service.Commands.Employers;
using JobPortal.Service.Helpers;
using JobPortal.Service.Queries.Employers;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JobPortal.WebApi.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class EmployerController : ApiController
    {
        private readonly IMediator _mediator;

        public EmployerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Метод получения профиля работодателя
        /// </summary>
        /// <param name="id">Идентификатор работодателя</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var response = await _mediator.Send(new GetEmployerQuery {Id = id});
            return response.IsSucceeded
                ? Ok(response.Data)
                : StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }

        /// <summary>
        /// Метод получения профиля работодателя для выбранного пользователя
        /// </summary>
        /// <param name="userId">Идентификатор пользователя</param>
        /// <returns></returns>
        [HttpGet("user/{userId}")]
        public async Task<IActionResult> GetByUserId(string userId)
        {
            var response = await _mediator.Send(new GetEmployerByUserIdQuery {UserId = userId});
            return response.IsSucceeded
                ? Ok(response.Data)
                : StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }

        /// <summary>
        /// Метод создания профиля работодателя
        /// </summary>
        /// <param name="command">Тело запроса</param>
        /// <returns></returns>
        [Authorize(Roles = Roles.AdminOrEmployer)]
        [HttpPost]
        public async Task<IActionResult> Create(CreateEmployerCommand command)
        {
            command.Username = User!.Identity!.Name;
            var response = await _mediator.Send(command);
            return response.IsSucceeded
                ? StatusCode(response.StatusCode, response.Data)
                : StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }

        /// <summary>
        /// Метод обновления профиля работодателя
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [Authorize(Roles = Roles.AdminOrEmployer)]
        [HttpPut]
        public async Task<IActionResult> Update(UpdateEmployerCommand command)
        {
            command.Username = User!.Identity!.Name;
            var response = await _mediator.Send(command);
            if (response.IsSucceeded)
            {
                return NoContent();
            }

            return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }

        /// <summary>
        /// Метод удаления профиля работадателя
        /// </summary>
        /// <param name="id">Идентификатор работодателя</param>
        /// <returns></returns>
        [Authorize(Roles = Roles.AdminOrEmployer)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var command = new DeleteEmployerCommand {Id = id, Username = User!.Identity!.Name};
            var response = await _mediator.Send(command);
            if (response.IsSucceeded)
            {
                return NoContent();
            }

            return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }
    }
}