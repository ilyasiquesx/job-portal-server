﻿using Microsoft.AspNetCore.Mvc;

namespace JobPortal.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ApiController : ControllerBase
    {
    }
}