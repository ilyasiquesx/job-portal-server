﻿using System.Threading.Tasks;
using JobPortal.Service.Commands.Vacancies;
using JobPortal.Service.Helpers;
using JobPortal.Service.Queries.Vacancies;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JobPortal.WebApi.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class VacancyController : ApiController
    {
        private readonly IMediator _mediator;

        public VacancyController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Метод создания новой вакансии
        /// </summary>
        /// <param name="command">Тело запроса</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = Roles.AdminOrEmployer)]
        public async Task<IActionResult> Create(CreateVacancyCommand command)
        {
            command.Username = User!.Identity!.Name;
            var response = await _mediator.Send(command);
            return response.IsSucceeded
                ? StatusCode(response.StatusCode, response.Data)
                : StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }

        /// <summary>
        /// Метод удаления вакансии
        /// </summary>
        /// <param name="id">Идентификатор вакансии</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.AdminOrEmployer)]
        public async Task<IActionResult> Delete(long id)
        {
            var command = new DeleteVacancyCommand {Id = id, Username = User!.Identity!.Name};
            var response = await _mediator.Send(command);
            if (response.IsSucceeded)
            {
                return NoContent();
            }

            return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }

        /// <summary>
        /// Метод обновления вакансии
        /// </summary>
        /// <param name="command">Тело запроса</param>
        /// <returns></returns>
        [HttpPut]
        [Authorize(Roles = Roles.AdminOrEmployer)]
        public async Task<IActionResult> Update(UpdateVacancyCommand command)
        {
            command.Username = User!.Identity!.Name;
            var response = await _mediator.Send(command);
            if (response.IsSucceeded)
            {
                return NoContent();
            }

            return StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }

        /// <summary>
        /// Метод получения списка вакансий, удовлетворяющих критериям фильтрации
        /// </summary>
        /// <param name="query">Параметры фильтрации</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetFiltered([FromQuery] GetVacanciesQuery query)
        {
            var response = await _mediator.Send(query);
            return response.IsSucceeded 
                ? Ok(response.Data) 
                : StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }

        /// <summary>
        /// Метод получения подробностей вакансии
        /// </summary>
        /// <param name="id">Идентификатор вакансии</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var query = new GetVacancyQuery {Id = id};
            var response = await _mediator.Send(query);
            return response.IsSucceeded 
                ? Ok(response.Data) 
                : StatusCode(response.StatusCode, new {errors = response.ErrorMessages});
        }
    }
}