﻿using System.Net;
using System.Net.Http.Json;
using System.Threading.Tasks;
using JobPortal.Service.Commands.Account;
using JobPortal.Service.Queries.Account;
using JobPortal.Service.Responses.Account;
using JobPortal.WebApi.Controllers;
using Xunit;

namespace JobPortal.Tests
{
    public class AccountControllerTests : IntegrationTest
    {
        private const string Account = "Account";

        [Fact]
        public async Task RegisterMethod_ShouldReturnSuccessStatusCode_WhenDataValid()
        {
            var registerCommand = new RegisterCommand
            {
                Username = "Admin",
                Password = "qwerty169",
                RoleName = "Admin"
            };
            var response =
                await HttpClient.PostAsJsonAsync($"/{Account}/{nameof(AccountController.Register)}", registerCommand);
            Assert.True(response.IsSuccessStatusCode);
        }

        [Fact]
        public async Task RegisterMethod_ShouldReturnAuthToken_WhenUserExist()
        {
            var registerCommand = new RegisterCommand
            {
                Username = nameof(RegisterMethod_ShouldReturnAuthToken_WhenUserExist),
                Password = "qwerty169",
                RoleName = "Employer"
            };
            var response =
                await HttpClient.PostAsJsonAsync($"/{Account}/{nameof(AccountController.Register)}", registerCommand);

            var loginData = await response.Content.ReadFromJsonAsync<LoginResponse>();

            Assert.NotNull(loginData);
            Assert.NotEmpty(loginData.AccessToken);
        }

        [Fact]
        public async Task RegisterMethod_ShouldReturnConflict_WhenUserExist()
        {
            var registerCommand = new RegisterCommand
            {
                Username = nameof(RegisterMethod_ShouldReturnConflict_WhenUserExist),
                Password = "qwerty169",
                RoleName = "Employer"
            };

            await HttpClient.PostAsJsonAsync($"/{Account}/{nameof(AccountController.Register)}", registerCommand);
            var conflictResult =
                await HttpClient.PostAsJsonAsync($"/{Account}/{nameof(AccountController.Register)}", registerCommand);

            Assert.Equal(HttpStatusCode.Conflict, conflictResult.StatusCode);
        }

        [Fact]
        public async Task LoginMethod_ShouldReturnAuthToken_WhenUserExist()
        {
            var registerCommand = new RegisterCommand
            {
                Username = nameof(LoginMethod_ShouldReturnAuthToken_WhenUserExist),
                Password = "qwerty169",
                RoleName = "Employer"
            };

            var loginCommand = new LoginQuery
            {
                Username = registerCommand.Username,
                Password = registerCommand.Password
            };
            
            await HttpClient.PostAsJsonAsync($"{Account}/{nameof(AccountController.Register)}", registerCommand);
            var loginResponse =
                await HttpClient.PostAsJsonAsync($"{Account}/{nameof(AccountController.Login)}", loginCommand);
            var loginData = await loginResponse.Content.ReadFromJsonAsync<LoginResponse>();
            
            Assert.NotNull(loginData);
            Assert.NotEmpty(loginData.AccessToken);
        }

        [Fact]
        public async Task LoginMethod_ShouldReturnNotFound_WhenUserDoesntExist()
        {
            var loginCommand = new LoginQuery
            {
                Username = nameof(LoginMethod_ShouldReturnNotFound_WhenUserDoesntExist),
                Password = "blank password"
            };
            var loginResponse =
                await HttpClient.PostAsJsonAsync($"{Account}/{nameof(AccountController.Login)}", loginCommand);

            Assert.Equal(HttpStatusCode.NotFound, loginResponse.StatusCode);
        }
    }
}