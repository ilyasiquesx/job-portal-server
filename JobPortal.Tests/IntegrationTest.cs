﻿using System.Linq;
using System.Net.Http;
using JobPortal.Repository.Context;
using JobPortal.WebApi;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace JobPortal.Tests
{
    public class IntegrationTest
    {
        protected readonly HttpClient HttpClient;

        protected IntegrationTest()
        {
            var appFactory = new WebApplicationFactory<Startup>()
                .WithWebHostBuilder(b => b.ConfigureServices(s =>
                {
                    var descriptor = s.SingleOrDefault(
                        d => d.ServiceType ==
                             typeof(DbContextOptions<AppDbContext>));
                    s.Remove(descriptor);
                    s.AddDbContext<AppDbContext>(options => { options.UseInMemoryDatabase("InMemoryDbForTesting"); });
                }));

            HttpClient = appFactory.CreateClient();
        }
    }
}