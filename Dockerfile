FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0-focal AS build
WORKDIR /src
COPY ["JobPortal.WebApi/JobPortal.WebApi.csproj", "./"]
RUN dotnet restore "JobPortal.WebApi.csproj"
COPY . .
RUN dotnet publish "JobPortal.WebApi/JobPortal.WebApi.csproj" -c Release -o /app/publish --no-cache

FROM base AS final
WORKDIR /app
COPY --from=build /app/publish .
ENTRYPOINT ["dotnet", "JobPortal.WebApi.dll"]


