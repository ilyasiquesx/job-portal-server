﻿using System;

namespace JobPortal.Domain.Abstractions
{
    public interface IAuditedEntity
    {
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}