﻿using System;
using System.ComponentModel.DataAnnotations;
using JobPortal.Domain.Abstractions;
using JobPortal.Domain.Common;

namespace JobPortal.Domain.Tables
{
    public class Vacancy : SalaryBase, IAuditedEntity
    {
        public long Id { get; set; }
        
        public Employer Employer { get; set; }
        
        public long EmployerId { get; set; }
        
        [MaxLength(64)]
        public string Name { get; set; }
        
        [MaxLength(1024)]
        public string Description { get; set; }
        
        [MaxLength(1024)]
        public string Requirements { get; set; }
        
        [MaxLength(1024)]
        public string Duties { get; set; }
        
        public Experience RequiredExperience { get; set; }
        
        public TypeOfEmployment TypeOfEmployment { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        public DateTime UpdatedAt { get; set; }
    }
}