﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using JobPortal.Domain.Abstractions;

namespace JobPortal.Domain.Tables
{
    public class Applicant : IAuditedEntity
    {
        public long Id { get; set; }
        
        public User User { get; set; }
        
        public string UserId { get; set; }
        
        [MaxLength(64)]
        public string FirstName { get; set; }
        
        [MaxLength(64)]
        public string LastName { get; set; }
        
        public DateTime DateOfBirth { get; set; }
        
        [MaxLength(64)]
        public string City { get; set; }
        
        [MaxLength(128)]
        public string ContactEmail { get; set; }
        
        [MaxLength(128)]
        public string ContactPhone { get; set; }
        
        [MaxLength(128)]
        public string InstagramLink { get; set; }
        
        [MaxLength(128)]
        public string FacebookLink { get; set; }
        
        [MaxLength(128)]
        public string VkLink { get; set; }
        
        [MaxLength(64)]
        public string Gender { get; set; }
        
        [MaxLength(64)]
        public string Citizenship { get; set; }
        
        public IEnumerable<Resume> Resumes { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        public DateTime UpdatedAt { get; set; }
    }
}