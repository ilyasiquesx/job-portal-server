﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using JobPortal.Domain.Abstractions;

namespace JobPortal.Domain.Tables
{
    public class Employer : IAuditedEntity
    {
        public long Id { get; set; }
        
        public User User { get; set; }
        
        public string UserId { get; set; }
        
        [MaxLength(64)]
        public string Name { get; set; }
        
        [MaxLength(128)]
        public string InfoUrl { get; set; }
        
        [MaxLength(128)]
        public string ContactEmail { get; set; }
        
        [MaxLength(128)]
        public string ContactPhone { get; set; }
        
        [MaxLength(1024)]
        public string Description { get; set; }
        
        public IEnumerable<Vacancy> Vacancies { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        public DateTime UpdatedAt { get; set; }
    }
}