﻿using Microsoft.AspNetCore.Identity;

namespace JobPortal.Domain.Tables
{
    public class User : IdentityUser
    {
        public Employer Employer { get; set; }
        
        public Applicant Applicant { get; set; }
    }
}