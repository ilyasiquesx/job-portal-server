﻿using System;
using System.ComponentModel.DataAnnotations;
using JobPortal.Domain.Abstractions;
using JobPortal.Domain.Common;

namespace JobPortal.Domain.Tables
{
    public class Resume : SalaryBase, IAuditedEntity
    {
        public long Id { get; set; }
        
        public Applicant Applicant { get; set; }
        
        public long ApplicantId { get; set; }
        
        [MaxLength(64)]
        public string Name { get; set; }
        
        [MaxLength(1024)]
        public string AboutMe { get; set; }
        
        public EducationStatus EducationStatus { get; set; }
        
        public Experience Experience { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        public DateTime UpdatedAt { get; set; }
    }
}