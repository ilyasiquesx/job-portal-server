﻿namespace JobPortal.Domain.Common
{
    public class SalaryBase
    {
        public decimal? SalaryFrom { get; set; }
        public decimal? SalaryTo { get; set; }
    }
}