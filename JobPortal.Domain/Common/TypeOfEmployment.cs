﻿namespace JobPortal.Domain.Common
{
    public enum TypeOfEmployment
    {
        FullTime = 0,
        PartTime = 1
    }
}