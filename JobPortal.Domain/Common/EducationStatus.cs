﻿namespace JobPortal.Domain.Common
{
    public enum EducationStatus
    {
        BachelorInProgress = 0,
        BachelorFinished = 1,
        SpecialityInProgress = 2,
        SpecialityFinished = 3,
        MasterInProgress = 4,
        MasterFinished = 5,
        GraduateInProgress = 6,
        GraduateFinished = 7
    }
}