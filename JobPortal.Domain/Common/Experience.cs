﻿namespace JobPortal.Domain.Common
{
    public enum Experience
    {
        WithoutExp = 0,
        FromOneToThree = 1,
        FromThreeToSix = 2,
        FromSix = 3
    }
}