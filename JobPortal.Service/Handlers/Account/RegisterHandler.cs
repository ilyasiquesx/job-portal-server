﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Service.Commands.Account;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Account;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace JobPortal.Service.Handlers.Account
{
    public class RegisterHandler : IRequestHandler<RegisterCommand, Response<RegisterResponse>>
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        private readonly IEnumerable<string> _allowedAdmins;

        public RegisterHandler(UserManager<User> userManager,
            IMapper mapper,
            RoleManager<IdentityRole> roleManager,
            IConfiguration configuration)
        {
            _userManager = userManager;
            _mapper = mapper;
            _roleManager = roleManager;
            var stringValueFromConfig = configuration[ConfigurationSections.AllowedAdmins];
            _allowedAdmins = JsonConvert.DeserializeObject<IEnumerable<string>>(stringValueFromConfig);
        }

        public async Task<Response<RegisterResponse>> Handle(RegisterCommand request,
            CancellationToken cancellationToken)
        {
            var registerResponse = new Response<RegisterResponse>();
            var user = await _userManager.FindByNameAsync(request.Username);
            if (user != null)
            {
                registerResponse.StatusCode = StatusCodes.Status409Conflict;
                registerResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserExistsMessage(request.Username));
                return registerResponse;
            }

            var role = await _roleManager.FindByNameAsync(request.RoleName);
            if (role == null)
            {
                registerResponse.StatusCode = StatusCodes.Status404NotFound;
                registerResponse.ErrorMessages.Add(ErrorMessages.Account.GetRoleNotFoundMessage(request.RoleName));
                return registerResponse;
            }

            if (role.Name == Roles.Admin && !_allowedAdmins.Contains(request.Username))
            {
                registerResponse.StatusCode = StatusCodes.Status403Forbidden;
                registerResponse.ErrorMessages.Add(
                    ErrorMessages.Account.GetUndefinedAdminAccountMessage(request.Username));
                return registerResponse;
            }

            user = _mapper.Map<User>(request);
            var result = await _userManager.CreateAsync(user, request.Password);
            if (!result.Succeeded)
            {
                registerResponse.StatusCode = StatusCodes.Status400BadRequest;
                registerResponse.ErrorMessages = result.Errors.Select(e => e.Description).ToList();
                return registerResponse;
            }

            var roleResult = await _userManager.AddToRoleAsync(user, role.Name);
            if (!roleResult.Succeeded)
            {
                registerResponse.StatusCode = StatusCodes.Status400BadRequest;
                registerResponse.ErrorMessages = result.Errors.Select(e => e.Description).ToList();
                return registerResponse;
            }

            var data = _mapper.Map<RegisterResponse>(user);
            registerResponse.SetData(data);
            return registerResponse;
        }
    }
}