﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using JobPortal.Domain.Tables;
using JobPortal.Service.Helpers;
using JobPortal.Service.Queries.Account;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Account;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace JobPortal.Service.Handlers.Account
{
    public class LoginHandler : IRequestHandler<LoginQuery, Response<LoginResponse>>
    {
        private readonly UserManager<User> _userManager;
        private readonly JwtOptions _jwtOptions;

        public LoginHandler(UserManager<User> userManager, IOptions<JwtOptions> jwtOptions)
        {
            _userManager = userManager;
            _jwtOptions = jwtOptions?.Value ?? throw new ArgumentNullException(nameof(jwtOptions));
        }

        public async Task<Response<LoginResponse>> Handle(LoginQuery request, CancellationToken cancellationToken)
        {
            var loginResponse = new Response<LoginResponse>();
            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                loginResponse.StatusCode = StatusCodes.Status404NotFound;
                loginResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return loginResponse;
            }

            var isPasswordCorrect = await _userManager.CheckPasswordAsync(user, request.Password);
            if (!isPasswordCorrect)
            {
                loginResponse.StatusCode = StatusCodes.Status401Unauthorized;
                loginResponse.ErrorMessages.Add(ErrorMessages.Account.GetWrongPasswordMessage(request.Username));
                return loginResponse;
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.UserName)
            };

            var roles = await _userManager.GetRolesAsync(user);
            claims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));

            var token = GenerateToken(claims);
            var data = new LoginResponse
            {
                Id = user.Id,
                Username = user.UserName,
                AccessToken = token,
                Roles = roles
            };
            loginResponse.SetData(data);
            return loginResponse;
        }

        private string GenerateToken(IEnumerable<Claim> claims)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(_jwtOptions.Issuer,
                _jwtOptions.Audience,
                claims,
                expires: DateTime.Now.AddMinutes(_jwtOptions.ExpiredMin),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}