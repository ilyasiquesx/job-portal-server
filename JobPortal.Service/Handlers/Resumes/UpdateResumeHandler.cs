﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Commands.Resumes;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Resumes
{
    public class UpdateResumeHandler : IRequestHandler<UpdateResumeCommand, Response<object>>
    {
        private readonly IResumeRepository _resumeRepository;
        private readonly IApplicantRepository _applicantRepository;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public UpdateResumeHandler(IResumeRepository resumeRepository,
            IApplicantRepository applicantRepository,
            UserManager<User> userManager,
            IMapper mapper)
        {
            _resumeRepository = resumeRepository;
            _applicantRepository = applicantRepository;
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<Response<object>> Handle(UpdateResumeCommand request, CancellationToken cancellationToken)
        {
            var updateResponse = new Response<object>();
            var resume = await _resumeRepository.GetByIdAsync(request.Id);
            if (resume == null)
            {
                updateResponse.StatusCode = StatusCodes.Status404NotFound;
                updateResponse.ErrorMessages.Add(ErrorMessages.Entity<Resume>.GetEntityNotFoundMessage(request.Id));
                return updateResponse;
            }

            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                updateResponse.StatusCode = StatusCodes.Status404NotFound;
                updateResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return updateResponse;
            }

            var applicant = await _applicantRepository.GetByUserIdAsync(user.Id);
            var userRoles = await _userManager.GetRolesAsync(user);
            if (userRoles.Contains(Roles.Admin) || applicant.Id == resume.ApplicantId)
            {
                _mapper.Map(request, resume);
                await _resumeRepository.UpdateAsync(resume);
                updateResponse.SetData(new object());
                return updateResponse;
            }

            updateResponse.StatusCode = StatusCodes.Status403Forbidden;
            updateResponse.ErrorMessages.Add(ErrorMessages.Entity<Resume>.GetForbidToModifyEntityMessage(resume.Id));
            return updateResponse;
        }
    }
}