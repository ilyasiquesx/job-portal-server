﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Helpers;
using JobPortal.Service.Queries.Resumes;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Resumes;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace JobPortal.Service.Handlers.Resumes
{
    public class GetResumeHandler : IRequestHandler<GetResumeQuery, Response<GetResumeResponse>>
    {
        private readonly IResumeRepository _resumeRepository;
        private readonly IMapper _mapper;

        public GetResumeHandler(IResumeRepository resumeRepository, IMapper mapper)
        {
            _resumeRepository = resumeRepository;
            _mapper = mapper;
        }

        public async Task<Response<GetResumeResponse>> Handle(GetResumeQuery request, CancellationToken cancellationToken)
        {
            var getResponse = new Response<GetResumeResponse>();
            var resume = await _resumeRepository.GetByIdAsync(request.Id);
            if (resume == null)
            {
                getResponse.StatusCode = StatusCodes.Status404NotFound;
                getResponse.ErrorMessages.Add(ErrorMessages.Entity<Resume>.GetEntityNotFoundMessage(request.Id));
                return getResponse;
            }
            
            _resumeRepository.LoadExplicit(resume);
            var data = _mapper.Map<GetResumeResponse>(resume);
            getResponse.SetData(data);
            return getResponse;
        }
    }
}