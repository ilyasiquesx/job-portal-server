﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Commands.Resumes;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Resumes;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Resumes
{
    public class CreateResumeHandler : IRequestHandler<CreateResumeCommand, Response<CreateResumeResponse>>
    {
        private readonly IApplicantRepository _applicantRepository;
        private readonly IResumeRepository _resumeRepository;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public CreateResumeHandler(UserManager<User> userManager, 
            IResumeRepository resumeRepository, 
            IApplicantRepository applicantRepository, 
            IMapper mapper)
        {
            _userManager = userManager;
            _resumeRepository = resumeRepository;
            _applicantRepository = applicantRepository;
            _mapper = mapper;
        }

        public async Task<Response<CreateResumeResponse>> Handle(CreateResumeCommand request, CancellationToken cancellationToken)
        {
            var createResponse = new Response<CreateResumeResponse>();
            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                createResponse.StatusCode = StatusCodes.Status404NotFound;
                createResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return createResponse;
            }

            var applicant = await _applicantRepository.GetByUserIdAsync(user.Id);
            if (applicant == null)
            {
                createResponse.StatusCode = StatusCodes.Status404NotFound;
                createResponse.ErrorMessages.Add(ErrorMessages.Entity<Applicant>.GetEntityNotFoundByUserIdMessage(user.Id));
                return createResponse;
            }

            var resume = _mapper.Map<Resume>(request);
            resume.Applicant = applicant;
            await _resumeRepository.CreateAsync(resume);
            var data = new CreateResumeResponse {Id = resume.Id};
            createResponse.StatusCode = StatusCodes.Status201Created;
            createResponse.SetData(data);
            return createResponse;
        }
    }
}