﻿using System.Threading;
using System.Threading.Tasks;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Commands.Resumes;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Resumes
{
    public class DeleteResumeHandler : IRequestHandler<DeleteResumeCommand, Response<object>>
    {
        private readonly IApplicantRepository _applicantRepository;
        private readonly IResumeRepository _resumeRepository;
        private readonly UserManager<User> _userManager;

        public DeleteResumeHandler(IResumeRepository resumeRepository, IApplicantRepository applicantRepository,
            UserManager<User> userManager)
        {
            _resumeRepository = resumeRepository;
            _applicantRepository = applicantRepository;
            _userManager = userManager;
        }

        public async Task<Response<object>> Handle(DeleteResumeCommand request, CancellationToken cancellationToken)
        {
            var deleteResponse = new Response<object>();
            var resume = await _resumeRepository.GetByIdAsync(request.Id);
            if (resume == null)
            {
                deleteResponse.StatusCode = StatusCodes.Status404NotFound;
                deleteResponse.ErrorMessages.Add(ErrorMessages.Entity<Resume>.GetEntityNotFoundMessage(request.Id));
                return deleteResponse;
            }

            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                deleteResponse.StatusCode = StatusCodes.Status404NotFound;
                deleteResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return deleteResponse;
            }

            var userRoles = await _userManager.GetRolesAsync(user);
            var applicant = await _applicantRepository.GetByUserIdAsync(user.Id);
            if (userRoles.Contains(Roles.Admin) || applicant.Id == resume.ApplicantId)
            {
                await _resumeRepository.DeleteAsync(resume);
                deleteResponse.SetData(new object());
                return deleteResponse;
            }

            deleteResponse.StatusCode = StatusCodes.Status403Forbidden;
            deleteResponse.ErrorMessages.Add(ErrorMessages.Entity<Resume>.GetForbidToModifyEntityMessage(resume.Id));
            return deleteResponse;
        }
    }
}