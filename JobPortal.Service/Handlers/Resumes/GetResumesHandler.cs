﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Repository.Abstractions;
using JobPortal.Repository.Models;
using JobPortal.Service.Extensions;
using JobPortal.Service.Queries.Resumes;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Resumes;
using MediatR;

namespace JobPortal.Service.Handlers.Resumes
{
    public class GetResumesHandler : IRequestHandler<GetResumesQuery, Response<GetResumesResponse>>
    {
        private readonly IApplicantRepository _applicantRepository;
        private readonly IResumeRepository _resumeRepository;
        private readonly IMapper _mapper;

        public GetResumesHandler(IResumeRepository resumeRepository, IMapper mapper, IApplicantRepository applicantRepository)
        {
            _resumeRepository = resumeRepository;
            _mapper = mapper;
            _applicantRepository = applicantRepository;
        }

        public Task<Response<GetResumesResponse>> Handle(GetResumesQuery request, CancellationToken cancellationToken)
        {
            var getResponse = new Response<GetResumesResponse>();
            // Filtering
            var filter = _mapper.Map<ResumeFilterModel>(request.Filtering);
            var filteredVacancies = _resumeRepository.GetFilteredResumes(filter);
            // Sorting
            filteredVacancies = filteredVacancies.SortCollection(request.Sorting);
            // Pagination
            var (resumes, page, pages) = filteredVacancies.PaginateCollection(request.Pagination);
            // Load related data
            var applicantIds = resumes.Select(r => r.ApplicantId).Distinct();
            var _ = _applicantRepository.GetApplicantsByIdList(applicantIds).ToList();
            
            var mappedCollection = _mapper.Map<IEnumerable<IndexResumeExtendedModel>>(resumes);
            var data = new GetResumesResponse
            {
                Resumes = mappedCollection,
                Page = page,
                Pages = pages
            };
            getResponse.SetData(data);
            return Task.FromResult(getResponse);
        }
    }
}