﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Commands.Vacancies;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Vacancies
{
    public class UpdateVacancyHandler : IRequestHandler<UpdateVacancyCommand, Response<object>>
    {
        private readonly IEmployerRepository _employerRepository;
        private readonly IVacancyRepository _vacancyRepository;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public UpdateVacancyHandler(UserManager<User> userManager, IEmployerRepository employerRepository,
            IVacancyRepository vacancyRepository, IMapper mapper)
        {
            _userManager = userManager;
            _employerRepository = employerRepository;
            _vacancyRepository = vacancyRepository;
            _mapper = mapper;
        }

        public async Task<Response<object>> Handle(UpdateVacancyCommand request, CancellationToken cancellationToken)
        {
            var updateResponse = new Response<object>();
            var vacancy = await _vacancyRepository.GetByIdAsync(request.Id);
            if (vacancy == null)
            {
                updateResponse.StatusCode = StatusCodes.Status404NotFound;
                updateResponse.ErrorMessages.Add(ErrorMessages.Entity<Vacancy>.GetEntityNotFoundMessage(request.Id));
                return updateResponse;
            }

            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                updateResponse.StatusCode = StatusCodes.Status404NotFound;
                updateResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return updateResponse;
            }

            var employer = await _employerRepository.GetByUserIdAsync(user.Id);
            var userRoles = await _userManager.GetRolesAsync(user);
            if (userRoles.Contains(Roles.Admin) || vacancy.EmployerId == employer.Id)
            {
                _mapper.Map(request, vacancy);
                await _vacancyRepository.UpdateAsync(vacancy);
                updateResponse.SetData(new object());
                return updateResponse;
            }

            updateResponse.StatusCode = StatusCodes.Status403Forbidden;
            updateResponse.ErrorMessages.Add(ErrorMessages.Entity<Vacancy>.GetForbidToModifyEntityMessage(vacancy.Id));
            return updateResponse;
        }
    }
}