﻿using System.Threading;
using System.Threading.Tasks;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Commands.Vacancies;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Vacancies
{
    public class DeleteVacancyHandler : IRequestHandler<DeleteVacancyCommand, Response<object>>
    {
        private readonly IVacancyRepository _vacancyRepository;
        private readonly IEmployerRepository _employerRepository;
        private readonly UserManager<User> _userManager;

        public DeleteVacancyHandler(IVacancyRepository vacancyRepository, UserManager<User> userManager,
            IEmployerRepository employerRepository)
        {
            _vacancyRepository = vacancyRepository;
            _userManager = userManager;
            _employerRepository = employerRepository;
        }

        public async Task<Response<object>> Handle(DeleteVacancyCommand request, CancellationToken cancellationToken)
        {
            var deleteResponse = new Response<object>();
            var vacancy = await _vacancyRepository.GetByIdAsync(request.Id);
            if (vacancy == null)
            {
                deleteResponse.StatusCode = StatusCodes.Status404NotFound;
                deleteResponse.ErrorMessages.Add(ErrorMessages.Entity<Vacancy>.GetEntityNotFoundMessage(request.Id));
                return deleteResponse;
            }

            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                deleteResponse.StatusCode = StatusCodes.Status404NotFound;
                deleteResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return deleteResponse;
            }

            var employer = await _employerRepository.GetByUserIdAsync(user.Id);
            var userRoles = await _userManager.GetRolesAsync(user);
            if (userRoles.Contains(Roles.Admin) || vacancy.EmployerId == employer.Id)
            {
                await _vacancyRepository.DeleteAsync(vacancy);
                deleteResponse.SetData(new object());
                return deleteResponse;
            }

            deleteResponse.StatusCode = StatusCodes.Status403Forbidden;
            deleteResponse.ErrorMessages.Add(ErrorMessages.Entity<Vacancy>.GetForbidToModifyEntityMessage(vacancy.Id));
            return deleteResponse;
        }
    }
}