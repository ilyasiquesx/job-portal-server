﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Helpers;
using JobPortal.Service.Queries.Vacancies;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Vacancies;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace JobPortal.Service.Handlers.Vacancies
{
    public class GetVacancyHandler : IRequestHandler<GetVacancyQuery, Response<GetVacancyResponse>>
    {
        private readonly IVacancyRepository _vacancyRepository;
        private readonly IMapper _mapper;

        public GetVacancyHandler(IVacancyRepository vacancyRepository, IMapper mapper)
        {
            _vacancyRepository = vacancyRepository;
            _mapper = mapper;
        }

        public  async Task<Response<GetVacancyResponse>> Handle(GetVacancyQuery request, CancellationToken cancellationToken)
        {
            var getVacancyResponse = new Response<GetVacancyResponse>();
            var vacancy = await _vacancyRepository.GetByIdAsync(request.Id);
            if (vacancy == null)
            {
                getVacancyResponse.StatusCode = StatusCodes.Status404NotFound;
                getVacancyResponse.ErrorMessages.Add(ErrorMessages.Entity<Vacancy>.GetEntityNotFoundMessage(request.Id));
                return getVacancyResponse;
            }
            
            _vacancyRepository.LoadExplicit(vacancy);
            var data = _mapper.Map<GetVacancyResponse>(vacancy);
            getVacancyResponse.SetData(data);
            return getVacancyResponse;
        }
    }
}