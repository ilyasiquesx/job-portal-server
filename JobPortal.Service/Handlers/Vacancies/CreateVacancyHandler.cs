﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Commands.Vacancies;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Vacancies;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Vacancies
{
    public class CreateVacancyHandler : IRequestHandler<CreateVacancyCommand, Response<CreateVacancyResponse>>
    {
        private readonly UserManager<User> _userManager;
        private readonly IEmployerRepository _employerRepository;
        private readonly IVacancyRepository _vacancyRepository;
        private readonly IMapper _mapper;

        public CreateVacancyHandler(IVacancyRepository vacancyRepository, IMapper mapper, UserManager<User> userManager, IEmployerRepository employerRepository)
        {
            _vacancyRepository = vacancyRepository;
            _mapper = mapper;
            _userManager = userManager;
            _employerRepository = employerRepository;
        }

        public async Task<Response<CreateVacancyResponse>> Handle(CreateVacancyCommand request, CancellationToken cancellationToken)
        {
            var createResponse = new Response<CreateVacancyResponse>();
            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                createResponse.StatusCode = StatusCodes.Status404NotFound;
                createResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return createResponse;
            }

            var employer = await _employerRepository.GetByUserIdAsync(user.Id);
            if (employer == null)
            {
                createResponse.StatusCode = StatusCodes.Status404NotFound;
                createResponse.ErrorMessages.Add(ErrorMessages.Entity<Employer>.GetEntityNotFoundByUserIdMessage(user.Id));
                return createResponse;
            }

            var vacancy = _mapper.Map<Vacancy>(request);
            vacancy.Employer = employer;
            await _vacancyRepository.CreateAsync(vacancy);
            var data = new CreateVacancyResponse {Id = vacancy.Id};
            createResponse.StatusCode = StatusCodes.Status201Created;
            createResponse.SetData(data);
            return createResponse;
        }
    }
}