﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Repository.Abstractions;
using JobPortal.Repository.Models;
using JobPortal.Service.Extensions;
using JobPortal.Service.Queries.Vacancies;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Vacancies;
using MediatR;

namespace JobPortal.Service.Handlers.Vacancies
{
    public class GetVacanciesHandler : IRequestHandler<GetVacanciesQuery, Response<GetVacanciesResponse>>
    {
        private readonly IVacancyRepository _vacancyRepository;
        private readonly IEmployerRepository _employerRepository;
        private readonly IMapper _mapper;

        public GetVacanciesHandler(IVacancyRepository vacancyRepository, IMapper mapper, IEmployerRepository employerRepository)
        {
            _vacancyRepository = vacancyRepository;
            _mapper = mapper;
            _employerRepository = employerRepository;
        }

        public Task<Response<GetVacanciesResponse>> Handle(GetVacanciesQuery request,
            CancellationToken cancellationToken)
        {
            var getResponse = new Response<GetVacanciesResponse>();
            // Filtering
            var filter = _mapper.Map<VacancyFilterModel>(request.Filtering);
            var filteredVacancies = _vacancyRepository.GetFilteredVacancies(filter);
            // Sorting
            filteredVacancies = filteredVacancies.SortCollection(request.Sorting);
            // Pagination
            var (vacancies, page, pages) = filteredVacancies.PaginateCollection(request.Pagination);
            // Loading related data
            var employerIds = vacancies.Select(e => e.EmployerId).Distinct();
            var _ = _employerRepository.GetEmployersByIdList(employerIds).ToList();
            
            var mappedCollection = _mapper.Map<IEnumerable<IndexVacancyExtendedModel>>(vacancies);
            var data = new GetVacanciesResponse
            {
                Vacancies = mappedCollection,
                Page = page,
                Pages = pages
            };

            getResponse.SetData(data);
            return Task.FromResult(getResponse);
        }
    }
}