﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Helpers;
using JobPortal.Service.Queries.Employers;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Employers;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Employers
{
    public class GetEmployerByUserIdHandler : IRequestHandler<GetEmployerByUserIdQuery, Response<GetEmployerResponse>>
    {
        private readonly UserManager<User> _userManager;
        private readonly IEmployerRepository _employerRepository;
        private readonly IMapper _mapper;

        public GetEmployerByUserIdHandler(UserManager<User> userManager, IEmployerRepository employerRepository, IMapper mapper)
        {
            _userManager = userManager;
            _employerRepository = employerRepository;
            _mapper = mapper;
        }

        public async Task<Response<GetEmployerResponse>> Handle(GetEmployerByUserIdQuery request, CancellationToken cancellationToken)
        {
            var getByIdResponse = new Response<GetEmployerResponse>();
            var user = await _userManager.FindByIdAsync(request.UserId);
            if (user == null)
            {
                getByIdResponse.StatusCode = StatusCodes.Status404NotFound;
                getByIdResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.UserId));
                return getByIdResponse;
            }
            
            var employer = await _employerRepository.GetByUserIdAsync(user.Id);
            if (employer == null)
            {
                getByIdResponse.StatusCode = StatusCodes.Status404NotFound;
                getByIdResponse.ErrorMessages.Add(ErrorMessages.Entity<Employer>.GetEntityNotFoundByUserIdMessage(user.Id));
                return getByIdResponse;
            }
            _employerRepository.LoadExplicit(employer);
            var data = _mapper.Map<GetEmployerResponse>(employer);
            getByIdResponse.SetData(data);
            return getByIdResponse;
        }
    }
}