﻿using System.Threading;
using System.Threading.Tasks;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Commands.Employers;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Employers
{
    public class DeleteEmployerHandler : IRequestHandler<DeleteEmployerCommand, Response<object>>
    {
        private readonly IEmployerRepository _employerRepository;
        private readonly UserManager<User> _userManager;

        public DeleteEmployerHandler(IEmployerRepository employerRepository, UserManager<User> userManager)
        {
            _employerRepository = employerRepository;
            _userManager = userManager;
        }

        public async Task<Response<object>> Handle(DeleteEmployerCommand request, CancellationToken cancellationToken)
        {
            var deleteResponse = new Response<object>();
            var employer = await _employerRepository.GetByIdAsync(request.Id);
            if (employer == null)
            {
                deleteResponse.StatusCode = StatusCodes.Status404NotFound;
                deleteResponse.ErrorMessages.Add(ErrorMessages.Entity<Employer>.GetEntityNotFoundMessage(request.Id));
                return deleteResponse;
            }

            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                deleteResponse.StatusCode = StatusCodes.Status404NotFound;
                deleteResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return deleteResponse;
            }

            var userRoles = await _userManager.GetRolesAsync(user);
            if (userRoles.Contains(Roles.Admin) || user.Id == employer.UserId)
            {
                await _employerRepository.DeleteAsync(employer);
                deleteResponse.SetData(new object());
                return deleteResponse;
            }

            deleteResponse.StatusCode = StatusCodes.Status403Forbidden;
            deleteResponse.ErrorMessages.Add(
                ErrorMessages.Entity<Employer>.GetForbidToModifyEntityMessage(employer.Id));
            return deleteResponse;
        }
    }
}