﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Commands.Employers;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Employers
{
    public class UpdateEmployerHandler : IRequestHandler<UpdateEmployerCommand, Response<object>>
    {
        private readonly UserManager<User> _userManager;
        private readonly IEmployerRepository _employerRepository;
        private readonly IMapper _mapper;

        public UpdateEmployerHandler(IEmployerRepository employerRepository, IMapper mapper,
            UserManager<User> userManager)
        {
            _employerRepository = employerRepository;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task<Response<object>> Handle(UpdateEmployerCommand request, CancellationToken cancellationToken)
        {
            var updateResponse = new Response<object>();
            var employer = await _employerRepository.GetByIdAsync(request.Id);
            if (employer == null)
            {
                updateResponse.StatusCode = StatusCodes.Status404NotFound;
                updateResponse.ErrorMessages.Add(ErrorMessages.Entity<Employer>.GetEntityNotFoundMessage(request.Id));
                return updateResponse;
            }

            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                updateResponse.StatusCode = StatusCodes.Status404NotFound;
                updateResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return updateResponse;
            }

            var userRoles = await _userManager.GetRolesAsync(user);
            if (userRoles.Contains(Roles.Admin) || user.Id == employer.UserId)
            {
                _mapper.Map(request, employer);
                await _employerRepository.UpdateAsync(employer);
                updateResponse.SetData(new object());
                return updateResponse;
            }

            updateResponse.StatusCode = StatusCodes.Status403Forbidden;
            updateResponse.ErrorMessages.Add(
                ErrorMessages.Entity<Employer>.GetForbidToModifyEntityMessage(employer.Id));
            return updateResponse;
        }
    }
}