﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Helpers;
using JobPortal.Service.Queries.Employers;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Employers;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Employers
{
    public class GetEmployerHandler : IRequestHandler<GetEmployerQuery, Response<GetEmployerResponse>>
    {
        private readonly IEmployerRepository _employerRepository;
        private readonly IMapper _mapper;

        public GetEmployerHandler(IEmployerRepository employerRepository, IMapper mapper)
        {
            _employerRepository = employerRepository;
            _mapper = mapper;
        }

        public async Task<Response<GetEmployerResponse>> Handle(GetEmployerQuery request, CancellationToken cancellationToken)
        {
            var getResult = new Response<GetEmployerResponse>();
            var employer = await _employerRepository.GetByIdAsync(request.Id);
            if (employer == null)
            {
                getResult.StatusCode = StatusCodes.Status404NotFound;
                getResult.ErrorMessages.Add(ErrorMessages.Entity<Employer>.GetEntityNotFoundMessage(request.Id));
                return getResult;
            }
            _employerRepository.LoadExplicit(employer);
            var data = _mapper.Map<GetEmployerResponse>(employer);
            getResult.SetData(data);
            return getResult;
        }
    }
}