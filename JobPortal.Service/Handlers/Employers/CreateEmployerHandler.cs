﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Commands.Employers;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Employers;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Employers
{
    public class CreateEmployerHandler : IRequestHandler<CreateEmployerCommand, Response<CreateEmployerResponse>>
    {
        private readonly IEmployerRepository _employerRepository;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public CreateEmployerHandler(IEmployerRepository employerRepository, IMapper mapper, UserManager<User> userManager)
        {
            _employerRepository = employerRepository;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task<Response<CreateEmployerResponse>> Handle(CreateEmployerCommand request, CancellationToken cancellationToken)
        {
            var createResponse = new Response<CreateEmployerResponse>();
            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                createResponse.StatusCode = StatusCodes.Status404NotFound;
                createResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return createResponse;
            }

            var employer = await _employerRepository.GetByUserIdAsync(user.Id);
            if (employer != null)
            {
                createResponse.StatusCode = StatusCodes.Status409Conflict;
                createResponse.ErrorMessages.Add(ErrorMessages.Entity<Employer>.GetEntityAlreadyExistsMessage(user.Id));
                return createResponse;
            }
            
            employer = _mapper.Map<Employer>(request);
            employer.UserId = user.Id;
            await _employerRepository.CreateAsync(employer);
            var data = new CreateEmployerResponse {Id = employer.Id};
            createResponse.StatusCode = StatusCodes.Status201Created;
            createResponse.SetData(data);
            return createResponse;
        }
    }
}