﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Helpers;
using JobPortal.Service.Queries.Applicants;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Applicants;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Applicants
{
    public class GetApplicantByUserIdHandler : IRequestHandler<GetApplicantByUserIdQuery, Response<GetApplicantResponse>>
    {
        private readonly IApplicantRepository _applicantRepository;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public GetApplicantByUserIdHandler(IApplicantRepository applicantRepository, UserManager<User> userManager, IMapper mapper)
        {
            _applicantRepository = applicantRepository;
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<Response<GetApplicantResponse>> Handle(GetApplicantByUserIdQuery request, CancellationToken cancellationToken)
        {
            var getByIdResponse = new Response<GetApplicantResponse>();
            var user = await _userManager.FindByIdAsync(request.UserId);
            if (user == null)
            {
                getByIdResponse.StatusCode = StatusCodes.Status404NotFound;
                getByIdResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.UserId));
                return getByIdResponse;
            }
            
            var applicant = await _applicantRepository.GetByUserIdAsync(user.Id);
            if (applicant == null)
            {
                getByIdResponse.StatusCode = StatusCodes.Status404NotFound;
                getByIdResponse.ErrorMessages.Add(ErrorMessages.Entity<Applicant>.GetEntityNotFoundByUserIdMessage(user.Id));
                return getByIdResponse;
            }
            _applicantRepository.LoadExplicit(applicant);
            var data = _mapper.Map<GetApplicantResponse>(applicant);
            getByIdResponse.SetData(data);
            return getByIdResponse;
        }
    }
}