﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Helpers;
using JobPortal.Service.Queries.Applicants;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Applicants;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace JobPortal.Service.Handlers.Applicants
{
    public class GetApplicantHandler : IRequestHandler<GetApplicantQuery, Response<GetApplicantResponse>>
    {
        private readonly IApplicantRepository _applicantRepository;
        private readonly IMapper _mapper;

        public GetApplicantHandler(IApplicantRepository applicantRepository, IMapper mapper)
        {
            _applicantRepository = applicantRepository;
            _mapper = mapper;
        }

        public async Task<Response<GetApplicantResponse>> Handle(GetApplicantQuery request, CancellationToken cancellationToken)
        {
            var getResult = new Response<GetApplicantResponse>();
            var applicant = await _applicantRepository.GetByIdAsync(request.Id);
            if (applicant == null)
            {
                getResult.StatusCode = StatusCodes.Status404NotFound;
                getResult.ErrorMessages.Add(ErrorMessages.Entity<Applicant>.GetEntityNotFoundMessage(request.Id));
                return getResult;
            }
            _applicantRepository.LoadExplicit(applicant);
            var data = _mapper.Map<GetApplicantResponse>(applicant);
            getResult.SetData(data);
            return getResult;
        }
    }
}