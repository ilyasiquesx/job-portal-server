﻿using System.Threading;
using System.Threading.Tasks;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Commands.Applicants;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Applicants
{
    public class DeleteApplicantHandler : IRequestHandler<DeleteApplicantCommand, Response<object>>
    {
        private readonly IApplicantRepository _applicantRepository;
        private readonly UserManager<User> _userManager;

        public DeleteApplicantHandler(IApplicantRepository applicantRepository, UserManager<User> userManager)
        {
            _applicantRepository = applicantRepository;
            _userManager = userManager;
        }

        public async Task<Response<object>> Handle(DeleteApplicantCommand request, CancellationToken cancellationToken)
        {
            var deleteResponse = new Response<object>();
            var applicant = await _applicantRepository.GetByIdAsync(request.Id);
            if (applicant == null)
            {
                deleteResponse.StatusCode = StatusCodes.Status404NotFound;
                deleteResponse.ErrorMessages.Add(ErrorMessages.Entity<Applicant>.GetEntityNotFoundMessage(request.Id));
                return deleteResponse;
            }

            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                deleteResponse.StatusCode = StatusCodes.Status404NotFound;
                deleteResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return deleteResponse;
            }

            var userRoles = await _userManager.GetRolesAsync(user);
            if (userRoles.Contains(Roles.Admin) || user.Id == applicant.UserId)
            {
                await _applicantRepository.DeleteAsync(applicant);
                deleteResponse.SetData(new object());
                return deleteResponse;
            }

            deleteResponse.StatusCode = StatusCodes.Status403Forbidden;
            deleteResponse.ErrorMessages.Add(
                ErrorMessages.Entity<Applicant>.GetForbidToModifyEntityMessage(applicant.Id));
            return deleteResponse;
        }
    }
}