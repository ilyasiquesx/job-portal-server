﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Commands.Applicants;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Applicants;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Applicants
{
    public class CreateApplicantHandler : IRequestHandler<CreateApplicantCommand, Response<CreateApplicantResponse>>
    {
        private readonly IApplicantRepository _applicantRepository;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public CreateApplicantHandler(IApplicantRepository applicantRepository, UserManager<User> userManager, IMapper mapper)
        {
            _applicantRepository = applicantRepository;
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<Response<CreateApplicantResponse>> Handle(CreateApplicantCommand request, CancellationToken cancellationToken)
        {
            var createResponse = new Response<CreateApplicantResponse>();
            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                createResponse.StatusCode = StatusCodes.Status404NotFound;
                createResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return createResponse;
            }

            var applicant = await _applicantRepository.GetByUserIdAsync(user.Id);
            if (applicant != null)
            {
                createResponse.StatusCode = StatusCodes.Status409Conflict;
                createResponse.ErrorMessages.Add(ErrorMessages.Entity<Applicant>.GetEntityAlreadyExistsMessage(user.Id));
                return createResponse;
            }
            
            applicant = _mapper.Map<Applicant>(request);
            applicant.UserId = user.Id;
            await _applicantRepository.CreateAsync(applicant);
            var data = new CreateApplicantResponse {Id = applicant.Id};
            createResponse.StatusCode = StatusCodes.Status201Created;
            createResponse.SetData(data);
            return createResponse;
        }
    }
}