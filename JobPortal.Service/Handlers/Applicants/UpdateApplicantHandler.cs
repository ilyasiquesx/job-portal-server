﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Service.Commands.Applicants;
using JobPortal.Service.Helpers;
using JobPortal.Service.Responses;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Handlers.Applicants
{
    public class UpdateApplicantHandler : IRequestHandler<UpdateApplicantCommand, Response<object>>
    {
        private readonly IApplicantRepository _applicantRepository;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;

        public UpdateApplicantHandler(IApplicantRepository applicantRepository, IMapper mapper,
            UserManager<User> userManager)
        {
            _applicantRepository = applicantRepository;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task<Response<object>> Handle(UpdateApplicantCommand request, CancellationToken cancellationToken)
        {
            var updateResponse = new Response<object>();
            var applicant = await _applicantRepository.GetByIdAsync(request.Id);
            if (applicant == null)
            {
                updateResponse.StatusCode = StatusCodes.Status404NotFound;
                updateResponse.ErrorMessages.Add(ErrorMessages.Entity<Applicant>.GetEntityNotFoundMessage(request.Id));
                return updateResponse;
            }

            var user = await _userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                updateResponse.StatusCode = StatusCodes.Status404NotFound;
                updateResponse.ErrorMessages.Add(ErrorMessages.Account.GetUserNotFoundMessage(request.Username));
                return updateResponse;
            }

            var userRoles = await _userManager.GetRolesAsync(user);
            if (userRoles.Contains(Roles.Admin) || user.Id == applicant.UserId)
            {
                _mapper.Map(request, applicant);
                await _applicantRepository.UpdateAsync(applicant);
                updateResponse.SetData(new object());
                return updateResponse;
            }

            updateResponse.StatusCode = StatusCodes.Status403Forbidden;
            updateResponse.ErrorMessages.Add(
                ErrorMessages.Entity<Applicant>.GetForbidToModifyEntityMessage(applicant.Id));
            return updateResponse;
        }
    }
}