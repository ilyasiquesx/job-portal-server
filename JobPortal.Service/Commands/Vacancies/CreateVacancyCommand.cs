﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using JobPortal.Domain.Common;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Vacancies;
using MediatR;

namespace JobPortal.Service.Commands.Vacancies
{
    
    public class CreateVacancyCommand : IRequest<Response<CreateVacancyResponse>>
    {
        [JsonIgnore]
        [MaxLength(64)]
        public string Username { get; set; }
        
        [Required]
        [MaxLength(64)]
        public string Name { get; set; }
        
        [Required]
        [MaxLength(1024)]
        public string Description { get; set; }
        
        [Required]
        [MaxLength(1024)]
        public string Requirements { get; set; }
        
        [Required]
        [MaxLength(1024)]
        public string Duties { get; set; }
        
        public Experience RequiredExperience { get; set; }
        
        public TypeOfEmployment TypeOfEmployment { get; set; }
        
        public decimal? SalaryFrom { get; set; }
        
        public decimal? SalaryTo { get; set; }
    }
}