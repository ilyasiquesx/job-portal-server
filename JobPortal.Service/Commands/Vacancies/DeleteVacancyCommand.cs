﻿using JobPortal.Service.Responses;
using MediatR;

namespace JobPortal.Service.Commands.Vacancies
{
    public class DeleteVacancyCommand : IRequest<Response<object>>
    {
        public string Username { get; set; }
        
        public long Id { get; set; }
    }
}