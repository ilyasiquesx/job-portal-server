﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using JobPortal.Domain.Common;
using JobPortal.Service.Responses;
using MediatR;

namespace JobPortal.Service.Commands.Vacancies
{
    public class UpdateVacancyCommand : IRequest<Response<object>>
    {
        [JsonIgnore]
        [MaxLength(64)]
        public string Username { get; set; }
        
        [Required]
        public long Id { get; set; }
        
        [MaxLength(64)]
        [Required]
        public string Name { get; set; }
        
        [MaxLength(1024)]
        [Required]
        public string Description { get; set; }
        
        [MaxLength(1024)]
        [Required]
        public string Requirements { get; set; }
        
        [MaxLength(1024)]
        [Required]
        public string Duties { get; set; }
        
        public Experience RequiredExperience { get; set; }
        
        public TypeOfEmployment TypeOfEmployment { get; set; }
        
        public decimal? SalaryFrom { get; set; }
        
        public decimal? SalaryTo { get; set; }
    }
}