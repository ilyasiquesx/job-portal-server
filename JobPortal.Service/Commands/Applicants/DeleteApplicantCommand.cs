﻿using JobPortal.Service.Responses;
using MediatR;

namespace JobPortal.Service.Commands.Applicants
{
    public class DeleteApplicantCommand : IRequest<Response<object>>
    {
        public long Id { get; set; }
        
        public string Username { get; set; }
    }
}