﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using JobPortal.Service.Responses;
using MediatR;

namespace JobPortal.Service.Commands.Applicants
{
    public class UpdateApplicantCommand : IRequest<Response<object>>
    {
        [JsonIgnore]
        [MaxLength(64)]
        public string Username { get; set; }
        
        [Required]
        public long Id { get; set; }
        
        [MaxLength(64)]
        [Required]
        public string FirstName { get; set; }
        
        [MaxLength(64)] 
        [Required]
        public string LastName { get; set; }
        
        public DateTime DateOfBirth { get; set; }
        
        [MaxLength(64)] 
        public string City { get; set; }
        
        [MaxLength(128)] 
        public string InstagramLink { get; set; }
        
        [MaxLength(128)] 
        public string FacebookLink { get; set; }
        
        [MaxLength(128)] 
        public string VkLink { get; set; }
        
        [MaxLength(64)] 
        public string Gender { get; set; }
        
        [MaxLength(64)] 
        public string Citizenship { get; set; }
        
        [MaxLength(128)] 
        public string ContactEmail { get; set; }
        
        [MaxLength(128)] 
        public string ContactPhone { get; set; }
    }
}