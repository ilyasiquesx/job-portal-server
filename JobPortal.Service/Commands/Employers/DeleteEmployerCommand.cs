﻿using System.Text.Json.Serialization;
using JobPortal.Service.Responses;
using MediatR;

namespace JobPortal.Service.Commands.Employers
{
    public class DeleteEmployerCommand : IRequest<Response<object>>
    {
        public long Id { get; set; }
        
        public string Username { get; set; }
    }
}