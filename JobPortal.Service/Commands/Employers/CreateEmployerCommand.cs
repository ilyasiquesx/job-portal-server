﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Employers;
using MediatR;

namespace JobPortal.Service.Commands.Employers
{
    public class CreateEmployerCommand : IRequest<Response<CreateEmployerResponse>>
    {
        [JsonIgnore]
        [MaxLength(64)]
        public string Username { get; set; }
        
        [Required]
        [MaxLength(64)]
        public string Name { get; set; }
        
        [Required]
        [MaxLength(1024)]
        public string Description { get; set; }
        
        [MaxLength(64)]
        public string InfoUrl { get; set; }
        
        [MaxLength(128)]
        public string ContactEmail { get; set; }
        
        [MaxLength(128)] 
        public string ContactPhone { get; set; }
    }
}