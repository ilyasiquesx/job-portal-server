﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using JobPortal.Service.Responses;
using MediatR;

namespace JobPortal.Service.Commands.Employers
{
    public class UpdateEmployerCommand : IRequest<Response<object>>
    {
        [Required] 
        public long Id { get; set; }
        
        [Required]
        [MaxLength(64)]
        public string Name { get; set; }
        
        [MaxLength(1024)]
        public string Description { get; set; }
        
        [MaxLength(64)]
        public string InfoUrl { get; set; }
        
        [JsonIgnore] 
        [MaxLength(64)]
        public string Username { get; set; }
        
        [MaxLength(128)] 
        public string ContactEmail { get; set; }
        
        [MaxLength(128)] 
        public string ContactPhone { get; set; }
    }
}