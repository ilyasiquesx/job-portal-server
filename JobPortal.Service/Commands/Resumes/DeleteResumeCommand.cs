﻿using JobPortal.Service.Responses;
using MediatR;

namespace JobPortal.Service.Commands.Resumes
{
    public class DeleteResumeCommand : IRequest<Response<object>>
    {
        public string Username { get; set; }
        
        public long Id { get; set; }
    }
}