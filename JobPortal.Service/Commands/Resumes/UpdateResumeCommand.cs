﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using JobPortal.Domain.Common;
using JobPortal.Service.Responses;
using MediatR;

namespace JobPortal.Service.Commands.Resumes
{
    public class UpdateResumeCommand : IRequest<Response<object>>
    {
        [JsonIgnore]
        [MaxLength(64)]
        public string Username { get; set; }
        
        public long Id { get; set; }
        
        [Required]
        [MaxLength(64)]
        public string Name { get; set; }
        
        [Required]
        [MaxLength(1024)]
        public string AboutMe { get; set; }
        
        public decimal? SalaryFrom { get; set; }
        
        public decimal? SalaryTo { get; set; }
        
        public EducationStatus EducationStatus { get; set; }
        
        public Experience Experience { get; set; }
    }
}