﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using JobPortal.Domain.Common;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Resumes;
using MediatR;

namespace JobPortal.Service.Commands.Resumes
{
    public class CreateResumeCommand : IRequest<Response<CreateResumeResponse>>
    {
        [JsonIgnore] 
        [MaxLength(64)]
        public string Username { get; set; }
        
        [MaxLength(64)]
        [Required]
        public string Name { get; set; }
        
        [MaxLength(1024)]
        public string AboutMe { get; set; }
        
        public EducationStatus EducationStatus { get; set; }
        
        public Experience Experience { get; set; }
        
        public decimal? SalaryFrom { get; set; }
        
        public decimal? SalaryTo { get; set; }
    }
}