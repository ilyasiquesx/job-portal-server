﻿using System.ComponentModel.DataAnnotations;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Account;
using MediatR;

namespace JobPortal.Service.Commands.Account
{
    public class RegisterCommand : IRequest<Response<RegisterResponse>>
    {
        [Required]
        [MaxLength(64)]
        public string Username { get; set; }
        
        [Required]
        [MaxLength(64)]
        public string Password { get; set; }
        
        [EmailAddress]
        public string Email { get; set; }
        
        [Phone]
        public string PhoneNumber { get; set; }
        
        [MaxLength(32)]
        [Required]
        public string RoleName { get; set; }
    }
}