﻿using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Models;
using JobPortal.Service.Commands.Account;
using JobPortal.Service.Commands.Applicants;
using JobPortal.Service.Commands.Employers;
using JobPortal.Service.Commands.Resumes;
using JobPortal.Service.Commands.Vacancies;
using JobPortal.Service.Queries.Resumes;
using JobPortal.Service.Queries.Vacancies;
using JobPortal.Service.Responses.Account;
using JobPortal.Service.Responses.Applicants;
using JobPortal.Service.Responses.Employers;
using JobPortal.Service.Responses.Resumes;
using JobPortal.Service.Responses.Vacancies;

namespace JobPortal.Service.Helpers
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<RegisterCommand, User>();
            CreateMap<User, RegisterResponse>();
            CreateMap<CreateEmployerCommand, Employer>();
            CreateMap<Employer, IndexEmployerModel>();
            CreateMap<Employer, GetEmployerResponse>();
            CreateMap<UpdateEmployerCommand, Employer>();
            CreateMap<CreateVacancyCommand, Vacancy>();
            CreateMap<VacancyFiltering, VacancyFilterModel>();
            CreateMap<Vacancy, IndexVacancyModel>();
            CreateMap<Vacancy, GetVacancyResponse>();
            CreateMap<UpdateVacancyCommand, Vacancy>();
            CreateMap<CreateApplicantCommand, Applicant>();
            CreateMap<UpdateApplicantCommand, Applicant>();
            CreateMap<Applicant, IndexApplicantModel>();
            CreateMap<Applicant, GetApplicantResponse>();
            CreateMap<CreateResumeCommand, Resume>();
            CreateMap<UpdateResumeCommand, Resume>();
            CreateMap<ResumeFiltering, ResumeFilterModel>();
            CreateMap<Resume, GetResumeResponse>();
            CreateMap<Resume, IndexResumeModel>();
            CreateMap<Vacancy, IndexVacancyExtendedModel>();
            CreateMap<Resume, IndexResumeExtendedModel>();
        }
    }
}