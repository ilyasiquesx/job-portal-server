﻿namespace JobPortal.Service.Helpers
{
    public static class ConfigurationSections
    {
        public const string ConnectionString = "ConnectionString";
        public const string Jwt = "Jwt";
        public const string Environment = "Environment";
        public const string ElasticOptions = "ElasticOptions";
        public const string AllowedAdmins = "AllowedAdmins";
    }
}