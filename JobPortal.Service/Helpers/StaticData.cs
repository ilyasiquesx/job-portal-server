﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace JobPortal.Service.Helpers
{
    public static class StaticData
    {
        public static readonly List<IdentityRole> Roles = new()
        {
            new IdentityRole {Name = "Admin"},
            new IdentityRole {Name = "Applicant"},
            new IdentityRole {Name = "Employer"}
        };
    }

    public static class Roles
    {
        public const string Admin = "Admin";
        public const string Employer = "Employer";
        public const string Applicant = "Applicant";
        public const string AdminOrEmployer = "Admin, Employer";
        public const string AdminOrApplicant = "Admin, Applicant";
    }
}