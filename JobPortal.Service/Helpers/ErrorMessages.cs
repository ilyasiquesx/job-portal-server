﻿namespace JobPortal.Service.Helpers
{
    public static class ErrorMessages
    {
        public static class Account
        {
            public static string GetUserNotFoundMessage(string username)
            {
                return $"User not found. Requested username: {username}.";
            }

            public static string GetWrongPasswordMessage(string username)
            {
                return $"Wrong password for account. Username: {username}.";
            }

            public static string GetUserExistsMessage(string username)
            {
                return $"User already exists. Requested username: {username}.";
            }

            public static string GetRoleNotFoundMessage(string roleName)
            {
                return $"Role not found. Requested role: {roleName}";
            }

            public static string GetUndefinedAdminAccountMessage(string username)
            {
                return $"This username: {username} can't use the admin role.";
            }
        }

        public static class Entity<T>
        {
            public static string GetEntityNotFoundMessage(long id)
            {
                return $"{typeof(T).Name} not found. Requested {typeof(T).Name} id: {id}.";
            }

            public static string GetEntityNotFoundByUserIdMessage(string userId)
            {
                return $"{typeof(T).Name} not found for this user. User id: {userId}";
            }

            public static string GetEntityAlreadyExistsMessage(string userId)
            {
                return $"{typeof(T).Name} already exist for user. User id: {userId}";
            }

            public static string GetForbidToModifyEntityMessage(long id)
            {
                return $"You have no access to modify this {typeof(T).Name}. Requested {typeof(T).Name} id: {id}";
            }
        } 
    }
}