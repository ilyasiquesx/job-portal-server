﻿namespace JobPortal.Service.Responses.Vacancies
{
    public class CreateVacancyResponse
    {
        public long Id { get; set; }
    }
}