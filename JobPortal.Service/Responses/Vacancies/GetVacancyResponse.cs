﻿using JobPortal.Domain.Common;
using JobPortal.Service.Responses.Employers;

namespace JobPortal.Service.Responses.Vacancies
{
    public class GetVacancyResponse : IndexVacancyExtendedModel
    {
        public string Description { get; set; }
        public string Requirements { get; set; }
        public string Duties { get; set; }
        public Experience RequiredExperience { get; set; }
        public TypeOfEmployment TypeOfEmployment { get; set; }
    }
}