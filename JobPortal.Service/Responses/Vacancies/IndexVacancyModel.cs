﻿using System;

namespace JobPortal.Service.Responses.Vacancies
{
    public class IndexVacancyModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public decimal? SalaryFrom { get; set; }
        public decimal? SalaryTo { get; set; }
    }
}