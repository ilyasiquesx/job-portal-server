﻿using System.Collections.Generic;

namespace JobPortal.Service.Responses.Vacancies
{
    public class GetVacanciesResponse
    {
        public int Page { get; set; }
        public int Pages { get; set; }
        public IEnumerable<IndexVacancyExtendedModel> Vacancies { get; set; }
    }
}