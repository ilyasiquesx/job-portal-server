﻿using JobPortal.Service.Responses.Employers;

namespace JobPortal.Service.Responses.Vacancies
{
    public class IndexVacancyExtendedModel : IndexVacancyModel
    {
        public IndexEmployerModel Employer { get; set; }
    }
}