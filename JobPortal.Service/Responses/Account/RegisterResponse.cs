﻿namespace JobPortal.Service.Responses.Account
{
    public class RegisterResponse
    {
        public string Id { get; set; }
        public string Username { get; set; }
    }
}