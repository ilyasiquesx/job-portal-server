﻿using System.Collections.Generic;

namespace JobPortal.Service.Responses.Account
{
    public class LoginResponse
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string AccessToken { get; set; }
        public IEnumerable<string> Roles { get; set; }
    }
}