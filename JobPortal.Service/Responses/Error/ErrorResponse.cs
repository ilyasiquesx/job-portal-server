﻿using System;

namespace JobPortal.Service.Responses.Error
{
    public class ErrorResponse
    {
        public string TraceIdentifier { get; set; }
    }
}