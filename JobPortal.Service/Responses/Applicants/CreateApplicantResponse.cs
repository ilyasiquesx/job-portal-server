﻿namespace JobPortal.Service.Responses.Applicants
{
    public class CreateApplicantResponse
    {
        public long Id { get; set; }
    }
}