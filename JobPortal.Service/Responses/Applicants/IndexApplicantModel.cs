﻿using System;

namespace JobPortal.Service.Responses.Applicants
{
    public class IndexApplicantModel
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string City { get; set; }
    }
}