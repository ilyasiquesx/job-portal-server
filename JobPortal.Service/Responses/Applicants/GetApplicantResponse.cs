﻿using System;
using System.Collections.Generic;
using JobPortal.Service.Responses.Resumes;

namespace JobPortal.Service.Responses.Applicants
{
    public class GetApplicantResponse : IndexApplicantModel
    {
        public string InstagramLink { get; set; }
        public string FacebookLink { get; set; }
        public string VkLink { get; set; }
        public string Gender { get; set; }
        public string Citizenship { get; set; }
        public IEnumerable<IndexResumeModel> Resumes { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreateAt { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
    }
}