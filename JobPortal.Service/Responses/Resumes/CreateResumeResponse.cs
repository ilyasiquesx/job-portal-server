﻿namespace JobPortal.Service.Responses.Resumes
{
    public class CreateResumeResponse
    {
        public long Id { get; set; }
    }
}