﻿using System.Collections.Generic;

namespace JobPortal.Service.Responses.Resumes
{
    public class GetResumesResponse
    {
        public int Page { get; set; }
        public int Pages { get; set; }
        public IEnumerable<IndexResumeExtendedModel> Resumes { get; set; }
    }
}