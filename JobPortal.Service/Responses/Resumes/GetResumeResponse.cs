﻿using System;
using JobPortal.Domain.Common;
using JobPortal.Service.Responses.Applicants;

namespace JobPortal.Service.Responses.Resumes
{
    public class GetResumeResponse : IndexResumeExtendedModel
    {
        public string AboutMe { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public EducationStatus EducationStatus { get; set; }
        public Experience Experience { get; set; }
    }
}