﻿using JobPortal.Service.Responses.Applicants;

namespace JobPortal.Service.Responses.Resumes
{
    public class IndexResumeExtendedModel : IndexResumeModel
    {
        public IndexApplicantModel Applicant { get; set; }
    }
}