﻿using System.Collections.Generic;
using JobPortal.Service.Responses.Vacancies;

namespace JobPortal.Service.Responses.Employers
{
    public class GetEmployerResponse : IndexEmployerModel
    {
        public string Description { get; set; }
        public string InfoUrl { get; set; }
        public string UserId { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public IEnumerable<IndexVacancyModel> Vacancies { get; set; }
    }
}