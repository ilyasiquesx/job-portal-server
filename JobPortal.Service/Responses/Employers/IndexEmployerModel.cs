﻿namespace JobPortal.Service.Responses.Employers
{
    public class IndexEmployerModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
    }
}