﻿using System.Collections.Generic;

namespace JobPortal.Service.Responses
{
    public class Response<T>
    {
        public int StatusCode { get; set; }
        public bool IsSucceeded { get; set; }
        public List<string> ErrorMessages { get; set; } = new List<string>();
        public T Data { get; set; }

        public void SetData(T data)
        {
            IsSucceeded = true;
            Data = data;
        }
    }
}