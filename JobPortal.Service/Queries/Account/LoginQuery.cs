﻿using System.ComponentModel.DataAnnotations;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Account;
using MediatR;

namespace JobPortal.Service.Queries.Account
{
    public class LoginQuery : IRequest<Response<LoginResponse>>
    {
        [Required]
        public string Username { get; set; }
        
        [Required]
        public string Password { get; set; }
    }
}