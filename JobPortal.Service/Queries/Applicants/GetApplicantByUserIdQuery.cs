﻿using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Applicants;
using MediatR;

namespace JobPortal.Service.Queries.Applicants
{
    public class GetApplicantByUserIdQuery : IRequest<Response<GetApplicantResponse>>
    {
        public string UserId { get; set; }
    }
}