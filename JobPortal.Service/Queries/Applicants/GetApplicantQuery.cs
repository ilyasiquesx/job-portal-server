﻿using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Applicants;
using MediatR;

namespace JobPortal.Service.Queries.Applicants
{
    public class GetApplicantQuery : IRequest<Response<GetApplicantResponse>>
    {
        public long Id { get; set; }
    }
}