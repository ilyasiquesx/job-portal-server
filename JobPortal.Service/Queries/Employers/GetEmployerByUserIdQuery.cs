﻿using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Employers;
using MediatR;

namespace JobPortal.Service.Queries.Employers
{
    public class GetEmployerByUserIdQuery : IRequest<Response<GetEmployerResponse>>
    {
        public string UserId { get; set; }
    }
}