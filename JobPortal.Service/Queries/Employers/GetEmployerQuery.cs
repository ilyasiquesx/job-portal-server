﻿using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Employers;
using MediatR;

namespace JobPortal.Service.Queries.Employers
{
    public class GetEmployerQuery : IRequest<Response<GetEmployerResponse>>
    {
        public long Id { get; set; }
    }
}