﻿using JobPortal.Domain.Common;
using JobPortal.Service.Common;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Resumes;
using MediatR;

namespace JobPortal.Service.Queries.Resumes
{
    public class GetResumesQuery : IRequest<Response<GetResumesResponse>>
    {
        public ResumeFiltering Filtering { get; set; }
        
        public Sorting Sorting { get; set; }
        
        public Pagination Pagination { get; set; } = new Pagination();
    }
    
    public class ResumeFiltering
    {
        public string Name { get; set; }
        public long? ApplicantId { get; set; }
        public Experience? Experience { get; set; }
        public decimal? SalaryFrom { get; set; }
        public decimal? SalaryTo { get; set; }
    }
}