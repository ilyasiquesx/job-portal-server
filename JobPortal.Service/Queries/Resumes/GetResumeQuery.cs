﻿using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Resumes;
using MediatR;

namespace JobPortal.Service.Queries.Resumes
{
    public class GetResumeQuery : IRequest<Response<GetResumeResponse>>
    {
        public long Id { get; set; }
    }
}