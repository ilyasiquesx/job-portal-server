﻿using System;
using JobPortal.Domain.Common;
using JobPortal.Service.Common;
using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Vacancies;
using MediatR;

namespace JobPortal.Service.Queries.Vacancies
{
    public class GetVacanciesQuery : IRequest<Response<GetVacanciesResponse>>
    {
        public VacancyFiltering Filtering { get; set; }
        
        public Sorting Sorting { get; set; }
        
        public Pagination Pagination { get; set; } = new Pagination();
    }
    
    public class VacancyFiltering
    {
        public string Name { get; set; }
        public long? EmployerId { get; set; }
        public Experience? RequiredExperience { get; set; }
        public decimal? SalaryFrom { get; set; }
        public decimal? SalaryTo { get; set; }
    }
}