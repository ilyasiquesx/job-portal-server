﻿using JobPortal.Service.Responses;
using JobPortal.Service.Responses.Vacancies;
using MediatR;

namespace JobPortal.Service.Queries.Vacancies
{
    public class GetVacancyQuery : IRequest<Response<GetVacancyResponse>>
    {
        public long Id { get; set; }
    }
}