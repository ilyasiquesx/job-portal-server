﻿namespace JobPortal.Service.Common
{
    public class Sorting
    {
        public SortingField? SortingField { get; set; }
        
        public bool ByAscending { get; set; }
    }

    public enum SortingField
    {
        SalaryFrom = 0,
        SalaryTo = 1,
        UpdatedAt = 2
    }
}