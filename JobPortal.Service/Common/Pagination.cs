﻿namespace JobPortal.Service.Common
{
    public class Pagination
    {
        public int Page { get; set; } = 1;
        
        private int _pageSize = 15;
        
        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = value > 25 ? 25 : value;
        }
    }
}