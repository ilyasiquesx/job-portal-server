﻿using System;
using System.Collections.Generic;
using System.Linq;
using JobPortal.Domain.Abstractions;
using JobPortal.Domain.Common;
using JobPortal.Service.Common;

namespace JobPortal.Service.Extensions
{
    public static class ListExtensions
    {
        public static IQueryable<T> SortCollection<T>(this IQueryable<T> items, Sorting sorting)
            where T : SalaryBase, IAuditedEntity
        {
            switch (sorting?.SortingField)
            {
                case SortingField.SalaryTo:
                    return sorting.ByAscending
                        ? items.OrderBy(i => i.SalaryTo.HasValue).ThenBy(i => i.SalaryTo)
                        : items.OrderByDescending(i => i.SalaryTo.HasValue).ThenByDescending(i => i.SalaryTo);
                case SortingField.UpdatedAt:
                    return sorting.ByAscending
                        ? items.OrderBy(i => i.UpdatedAt)
                        : items.OrderByDescending(i => i.UpdatedAt);
                case SortingField.SalaryFrom:
                    return sorting.ByAscending
                        ? items.OrderBy(i => i.SalaryFrom.HasValue).ThenBy(i => i.SalaryFrom)
                        : items.OrderByDescending(i => i.SalaryFrom.HasValue).ThenByDescending(i => i.SalaryFrom);
            }

            return items.OrderByDescending(i => i.UpdatedAt);
        }

        public static (IList<T> items, int page, int pages) PaginateCollection<T>(this IQueryable<T> items,
            Pagination pagination)
        {
            var itemsCount = items.Count();
            var pages = (int) Math.Ceiling((double) itemsCount / pagination.PageSize);
            if (itemsCount == 0)
            {
                return (items.ToList(),
                    pagination.Page,
                    pages);
            }

            if (pagination.Page > pages)
            {
                pagination.Page = pages;
            }

            items = items.Skip((pagination.Page - 1) * pagination.PageSize).Take(pagination.PageSize);
            return (items.ToList(),
                pagination.Page,
                pages);
        }
    }
}