﻿using System;
using System.Reflection;
using System.Text;
using AutoMapper;
using JobPortal.Domain.Tables;
using JobPortal.Repository.Abstractions;
using JobPortal.Repository.Context;
using JobPortal.Repository.Implementations;
using JobPortal.Service.Helpers;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Serilog.Sinks.Elasticsearch;

namespace JobPortal.Service.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration,
            IWebHostEnvironment webHostEnvironment)
        {
            services.AddOptions();
            services.Configure<JwtOptions>(configuration.GetSection(ConfigurationSections.Jwt));
            services.Configure<KestrelServerOptions>(opt => opt.AllowSynchronousIO = true);
            services.Configure<ElasticOptions>(configuration.GetSection(ConfigurationSections.ElasticOptions));
            services.AddMetrics();
            services.AddInternalLogging(configuration, webHostEnvironment);
            services.AddLogging(loggerBuilder => loggerBuilder.AddSerilog());
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    var jwtOptions = configuration.GetSection(ConfigurationSections.Jwt).Get<JwtOptions>();
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = jwtOptions.Issuer,
                        ValidAudience = jwtOptions.Audience,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.Key))
                    };
                });
            var connectionString = configuration[ConfigurationSections.ConnectionString];
            services.AddDbContext<AppDbContext>(o => o.UseNpgsql(connectionString));
            services.AddIdentity<User, IdentityRole>(opt =>
                {
                    opt.Password.RequireNonAlphanumeric = false;
                    opt.Password.RequireDigit = false;
                    opt.Password.RequireUppercase = false;
                })
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<IVacancyRepository, VacancyRepository>();
            services.AddTransient<IApplicantRepository, ApplicantRepository>();
            services.AddTransient<IEmployerRepository, EmployerRepository>();
            services.AddTransient<IResumeRepository, ResumeRepository>();
            var executingAssembly = Assembly.GetExecutingAssembly();
            services.AddAutoMapper(executingAssembly);
            services.AddMediatR(executingAssembly);

            return services;
        }

        private static IServiceCollection AddInternalLogging(this IServiceCollection services,
            IConfiguration configuration,
            IWebHostEnvironment webHostEnvironment)
        {
            var loggerConfiguration = new LoggerConfiguration()
                .Enrich
                .FromLogContext()
                .Enrich.WithMachineName()
                .WriteTo.Debug()
                .WriteTo.Console()
                .Enrich.WithProperty(ConfigurationSections.Environment, webHostEnvironment.EnvironmentName)
                .ReadFrom.Configuration(configuration);
            if (!webHostEnvironment.IsDevelopment())
            {
                var elasticUri = configuration.GetSection(ConfigurationSections.ElasticOptions).Get<ElasticOptions>()
                    ?.Uri;
                loggerConfiguration.WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(elasticUri))
                {
                    AutoRegisterTemplate = true,
                    IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name.ToLower().Replace(".", "-")}" +
                                  $"-{webHostEnvironment.EnvironmentName}"
                });
            }

            Log.Logger = loggerConfiguration.CreateLogger();
            return services;
        }
    }
}